pipeline {

    agent {
        node {
            label 'master'
            customWorkspace 'C:\\Jenkins\\workspace\\party_address_verification'
        }
    }

    environment {
        WS = 'C:\\Jenkins\\workspace\\party_address_verification'
    }

    stages {
        stage('Clone project') {
            steps {
                git branch: 'master', 
                    credentialsId: '5c537e31-0d16-4a79-92b3-33c10e0fdf77',
                    url: 'https://santiago_ramirez@bitbucket.org/santiago_ramirez/party-address-verification-test.git'
            }
        }
        stage('Read config file') {
            steps {
                script {
                    load String.format("%s\\framework\\config.groovy", WORKSPACE)
                }
            }
        }
        stage('Download attachments') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: env.email_credential_id, passwordVariable: 'email_pass', usernameVariable: 'email_user')]) {
                        output = bat(script: String.format(
                            "python %s\\workflows\\download_attachments.py %s %s %s %s %s %s",
                                WORKSPACE,
                                email_user,
                                email_pass,
                                env.email_iserver,
                                env.email_iport,
                                env.shared_folder,
                                env.customers
                            ), returnStdout: true
                        )

                        echo "${output}"
                    }
                }
            }
        }
        stage('Init data') {
            steps {
                script {
                    withCredentials([
                        usernamePassword(credentialsId: env.queue_credential_id, passwordVariable: 'pass', usernameVariable: 'user'),
                        usernamePassword(credentialsId: env.email_credential_id, passwordVariable: 'email_pass', usernameVariable: 'email_user')
                        ]) {
                        output = bat(script: String.format(
                            "python %s\\workflows\\dispatcher.py %s %s %s %s %s %s %s %s %s %s %s",
                                WORKSPACE,
                                env.queue_ip,
                                env.queue_port,
                                user,
                                pass,
                                env.queue_name,
                                env.shared_folder,
                                env.excel_sheet_name,
                                env.email_sserver,
                                env.email_sport,
                                email_user,
                                email_pass
                            ), returnStdout: true
                        )

                        echo "${output}"
                    }
                }
            }
        }
        stage('Validate addresses and get satellite images') {

            agent {
                node {
                    label 'agent_01'
                }
            }
            steps {
                script {
                    echo "This is branch a"
                    def hn1 = bat(script: String.format('hostname'), returnStdout: true)
                    echo hn1
                    withCredentials([usernamePassword(credentialsId: env.queue_credential_id, passwordVariable: 'pass', usernameVariable: 'user')]) {
                
                        output = bat(script: String.format(
                            "python %s\\workflows\\performer.py %s %s %s %s %s %s %s",
                            WORKSPACE,
                            env.queue_ip,
                            env.queue_port,
                            user,
                            pass,
                            env.queue_name,
                            env.queue_name_result,
                            WORKSPACE
                        ), returnStdout: true)

                        echo "${output}"

                    }
                }
            }     
        }
        stage('Generate report') {
            steps {
                script{
                    withCredentials([usernamePassword(credentialsId: env.queue_credential_id, passwordVariable: 'pass', usernameVariable: 'user')]) {
                        output= bat(script: String.format(
                            "python %s\\workflows\\generate_report.py %s %s %s %s %s",
                                WORKSPACE,
                                env.queue_ip,
                                env.queue_port,
                                user,
                                pass,
                                env.queue_name_result
                            ), returnStdout: true)

                        echo "${output}"
                    }
                }   
            }
        }
        stage('Send email to customer') {
            steps {
                script{
                    withCredentials([usernamePassword(credentialsId: env.email_credential_id, passwordVariable: 'email_pass', usernameVariable: 'email_user')]) {
                        echo "Send email"
                        output= bat(script: String.format(
                            "python %s\\workflows\\send_email.py %s %s %s %s %s",
                                WORKSPACE,
                                env.email_sserver,
                                env.email_sport,
                                email_user,
                                email_pass,
                                env.shared_folder
                            ), returnStdout: true)
                        echo "${output}"
                    }
                }
            }
        }
    }
    post { 
        always { 
            echo "Clean WS"
            cleanWs()
        }
    }
}


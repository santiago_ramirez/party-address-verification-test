from amqpstorm import Connection
from amqpstorm import Message
import pika

class Queue:
    queue_name = ''
    connection = None
    channel = None
    method_frame = None    


    def __init__(self, ip_address, port, username, password, queue_name):
        self.queue_name = queue_name
        #self.connection = Connection(ip_address, username, password, port)
        #self.channel = self.connection.channel()
        #self.channel.queue.declare(queue_name)
        credentials = pika.PlainCredentials(username, password)
        parameters = pika.ConnectionParameters(ip_address, port, credentials=credentials)
        connection = pika.BlockingConnection(parameters=parameters)
        channel = connection.channel()
        channel.queue_declare(queue=queue_name, durable=True)

    def read(self):
        #queue_item = self.channel.basic.get(queue=self.queue_name, no_ack=False)
        #if not queue_item:
            #print("Channel Empty.")

        self.method_frame, header_frame, queue_item = self.channel.basic_get(queue=self.queue_name, auto_ack=False)
        if not self.method_frame:
            print("Channel Empty.")

        return queue_item

    def write(self, body):
        #message = Message.create(self.channel, body=body, properties={})
        #message.publish(self.queue_name)

        self.channel.basic_publish('', self.queue_name, body, pika.BasicProperties(delivery_mode=2))

    def mark_as_read(self, queue_item):
        #self.channel.basic.ack(queue_item.method['delivery_tag'])
        self.channel.basic_ack(self.method_frame.delivery_tag)

    def close_connection(self):
        self.channel.close()
        self.connection.close()
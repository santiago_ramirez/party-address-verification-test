env.queue_name = "party_address_verification"
env.queue_name_result = "party_address_verification_result"
env.queue_ip =  "localhost"
env.queue_port = 5672
env.queue_credential_id = '67856766-0418-4e4b-aa86-c973d47fc489'

env.email_sserver = "smtp.gmail.com"
env.email_sport = 587
env.email_iserver = 'imap.gmail.com'
env.email_iport = 993
env.email_credential_id = '8e1391e3-aca8-4356-b882-9e20072269d8'

env.customers = 'santiago.ramirez@teaminternational.com' 
   

env.shared_folder = 'C:\\Jenkins\\workspace\\party_address_verification\\attachments'

env.excel_sheet_name = 'Sheet1'


env.python_dependencies = ['rpaframework', 'amqpstorm', 'opencv-python', 'geopy', 'requests']
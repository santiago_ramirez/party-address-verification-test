import pickle
import sys, os
from RPA.Excel.Files import Files
from RPA.FileSystem import FileSystem
from RPA.Email.ImapSmtp import ImapSmtp

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from framework.queue import Queue

arguments = sys.argv

queue_ip =  arguments[1]
queue_port = int(arguments[2])
queue_username = arguments[3]
queue_password = arguments[4]
queue_name = arguments[5]
shared_folder = arguments[6]
worksheet_name  = arguments[7]
email_sserver = arguments[8]
email_sport = int(arguments[9])
email_account = arguments[10]
email_password = arguments[11]

# connect to queue
queue = Queue(queue_ip, queue_port, queue_username, queue_password, queue_name)

data = []

try:
    # find excel files in party address verification folder
    fs = FileSystem()
    input_files = fs.find_files('{}/**/*.xlsx'.format(shared_folder))
    
    file = Files()
    
    for input_file in input_files:
        data = []
        file.open_workbook(input_file)
        try:
            # read data from excel file
            print('---Reading data from file: ' + str(input_file.path))
            data = file.read_worksheet_as_table(worksheet_name, header=True)

            # validate headers
            mandatory_headers = ['Address', 'City', 'State', 'Zip Code']
            if False in list(map(lambda x: x in data.columns, mandatory_headers)): 
                dir_name = os.path.dirname(os.path.dirname(input_file))
                customer = os.path.split(dir_name)[-1]
                mail = ImapSmtp()
                mail.authorize_smtp(email_account, email_password, email_sserver, email_sport)
                print('---Sending erro email to: {}'.format(customer))
                mail.send_message(
                    sender=email_account,
                    recipients=customer,
                    subject="Party address verification error",
                    body='''Hi,
Please check format in the file you send, it did not have expected columns
Thank you,
RPA Team''')
                error_message = 'Column headers must exist as following: {}'.format(mandatory_headers)
                raise Exception(error_message)

        except Exception as e:
            raise Exception(str(e))

        if data:
            for value in data:
                # refine data if present
                name = str(value['Name']).strip() if value['Name'] else None
                address = str(value['Address']).strip().replace(',','').replace('.','').replace('#', '') if value['Address'] else None
                city = str(value['City']).strip() if value['City'] else None
                state = str(value['State']).strip() if value['State'] else None
                zip_code = str(value['Zip Code']).strip() if value['Zip Code'] else None
                dir_name = os.path.dirname(os.path.dirname(input_file.path))

                # queue item body
                body = pickle.dumps({
                    'name': name,
                    'address': address,
                    'city': city,
                    'state': state,
                    'zip_code': zip_code,
                    'dir_name': dir_name
                    })

                print('---Uploading queue item: {}'.format(value))
                # upload queue item
                queue.write(body)
        file.close_workbook()
               
except Exception as e:
    queue.close_connection()
    raise Exception('Not possible to init data: ' + str(e))

queue.close_connection()


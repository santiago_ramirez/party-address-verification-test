import sys, os
import pickle
from RPA.Excel.Files import Files

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from framework.queue import Queue


arguments = sys.argv

queue_ip =  arguments[1]
queue_port = int(arguments[2])
queue_username = arguments[3]
queue_password = arguments[4]
queue_name_result = arguments[5]

def __append_row_to_worksheet(files, data):
    data_row = {
        'Name': data['name'] if 'name' in data else '',
        'Address':data['address'] if 'address' in data else '',
        'City': data['city'] if 'city' in data else '',
        'State': data['state'] if 'state' in data else '',
        'Zip code': data['zip_code'] if 'zip_code' in data else '',
        'Valid address': data['is_address_valid'] if 'is_address_valid' in data else '',
        'Satellite image': data['satellite_image'] if 'satellite_image' in data else '',
        'USPS address': data['USPS_address'] if 'USPS_address' in data else '',
        'Note': data['note'] if 'note' in data else ''    
    }

    files.append_rows_to_worksheet(data_row, header=True)


queue = Queue(queue_ip, queue_port, queue_username, queue_password, queue_name_result)

while True:
    try:
        queue_item = queue.read()
        if(not queue_item):
            break
        data = pickle.loads(queue_item)
        print('---Porcessing queue item {}'.format(data))
        queue.mark_as_read(queue_item)
    except Exception as e:
        print('Cannot read message from queue {}'.format(e))
        continue

    try:
        file_path = os.path.join(data['dir_name'], 'address_verification_report.xlsx')
        files = Files()

        if not os.path.exists(file_path):
            print('---create workbook {}'.format(file_path))
            f = files.create_workbook(file_path)
        else:
            print('---open workbook {}'.format(file_path))
            f = files.open_workbook(file_path)

        __append_row_to_worksheet(files, data)
        print('---Data appended to report')

        f.save()
    except Exception as e:
        print('Cannot create final report. {}'.format(e))

queue.close_connection()
import sys, os

from RPA.Email.ImapSmtp import ImapSmtp
from RPA.FileSystem import FileSystem

arguments = sys.argv

account = arguments[1]
password = arguments[2]
iserver = arguments[3]
iport = int(arguments[4])
shared_folder = arguments[5]
customers = arguments[6:]

try:
    mail = ImapSmtp()
    mail.authorize_imap(account, password, iserver, iport)

    fs = FileSystem()

    for index, customer in enumerate(customers):
        fs.create_directory(shared_folder)
        customer_folder = os.path.join(shared_folder, customer)
        fs.create_directory(customer_folder)
        customer_unique_folder = os.path.join(customer_folder, str(index))
        fs.create_directory(customer_unique_folder)

        list_files = mail.save_attachments(r'(UNSEEN SUBJECT "party address verification" FROM "{}")'.format(customer), customer_unique_folder)

        if list_files:
            print('---Attachments have been saved: {}'.format(list_files))
        else:
            print('---No emails/attachments for {}'.format(customer))
except Exception:
    raise Exception('---Cannot download party address verification attachments')
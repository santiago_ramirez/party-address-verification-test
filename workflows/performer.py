import sys, os
import time
import pickle
from RPA.Browser import Browser

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from framework.queue import Queue
from address_processing.get_satellite_image import SatelliteImage
from address_processing.validate_address import  ValidateAddress


arguments = sys.argv

queue_ip =  arguments[1]
queue_port = int(arguments[2])
queue_username = arguments[3]
queue_password = arguments[4]
queue_name = arguments[5]
queue_name_result = arguments[6]
workspace = arguments[7]

queue = Queue(queue_ip, queue_port, queue_username, queue_password, queue_name)
result_queue = Queue(queue_ip, queue_port, queue_username, queue_password, queue_name_result)

while True:
    try:
        queue_item = queue.read()
        
        if not queue_item:
            break
        
        data = pickle.loads(queue_item)
        queue.mark_as_read(queue_item)
        print('---Processing queue item: {}'.format(data))
        name = data['name']
        address = data['address']
        city = data['city']
        state = data['state']
        zip_code = data['zip_code']
        dir_name = data['dir_name']

        if address and city and state and zip_code:
            print('---Validating address in USPS')
            try:
                is_address_valid,address_validated,city_validated,state_validated,zip_code_validated = ValidateAddress().validate_addr(address, city, state, zip_code)
            except Exception as e:
                print(e)
                raise Exception('Error validating address in USPS')
            print('---Addres valid: ' + str(is_address_valid))

            image_name = ''
            if is_address_valid:
                print('---Getting satellite image')
                try:
                    image_name = SatelliteImage(workspace).generate_satellite_image('{}, {}, {}'.format(address_validated,city_validated,state_validated))
                except Exception as e:
                    print('---Error with mapbox API: ' + str(e))
                    print('---Google maps will be used')
                    try:
                        image_name = SatelliteImage(workspace).make_map_screenshot(city_validated, address_validated, state_validated, zip_code_validated)
                    except:
                        pass
                
                data['USPS_address'] = address_validated +', '+ city_validated + ', '+ state_validated + ' ' + zip_code_validated        
            else:
                data['USPS_address'] = ''     

            data['satellite_image'] = image_name
            data['is_address_valid'] = is_address_valid  
        else:
            data['note'] = 'Missing data'

        print('---Uploading result queue item: {}'.format(data))
        result_queue.write(pickle.dumps(data)) 
        print('--- Queue item processed successfuly')

    except Exception as e:
        print(e)
        data['note'] = 'Error validating address'
        result_queue.write(pickle.dumps(data))


queue.close_connection()
result_queue.close_connection()
import requests
import xml.etree.ElementTree as ET

class ValidateAddress:
    usps_api_url = 'https://secure.shippingapis.com/ShippingAPI.dll?API=Verify&XML='
    USERID = '606TEAMI0617'
    
    def get_usps_xml_template(self, addr, city, state, zip_code):
        xml_template = '''<AddressValidateRequest USERID="%s">
            <Revision>1</Revision>
            <Address ID="0">
            <Address1></Address1>
            <Address2>%s</Address2>
            <City>%s</City>
            <State>%s</State>
            <Zip5>%s</Zip5>
            <Zip4/>
            </Address>
            </AddressValidateRequest>''' % (self.USERID, addr, city, state, zip_code)

        return xml_template


    def validate_addr(self, address, city, state, zip_code):

        xml_body = self.get_usps_xml_template(address, city, state, zip_code)
        address_validated = ''
        city_validated = ''
        state_validated = ''
        zip_code_validated = ''
        try:
            print('---Requesting USPS api')
            response = requests.get(url = self.usps_api_url + xml_body, timeout= 10)
        except Exception as e:
            print('Error in USPS API request: ' + str(e))
        responseXml = ET.fromstring(response.text)
        try:
            address_validated = responseXml.find('Address').find('Address2').text
            city_validated = responseXml.find('Address').find('City').text
            state_validated = responseXml.find('Address').find('State').text
            zip_code_validated = responseXml.find('Address').find('Zip5').text
            valid_address = True
        except Exception:
            valid_address = False

        return valid_address, address_validated, city_validated, state_validated, zip_code_validated

    







        



from PIL import Image
import requests 
import shutil   
import os
from geopy.geocoders import Nominatim
from RPA.FileSystem import FileSystem
import urllib.request
from RPA.Browser import Browser
import time
from cv2 import cv2
import uuid
from random import randint
import shutil



class SatelliteImage:

    workspace = ''

    def __init__(self, workspace):
        self.workspace = workspace

    def generate_satellite_image(self, address):
        fs = FileSystem()

        # create folder to store satellite images if it dos not exists
        folder_exists = fs.does_directory_exist(self.workspace + '\\satellite_images')

        if not folder_exists:
            fs.create_directory(self.workspace + '\\satellite_images')
        
        access_token = 'pk.eyJ1Ijoic2FudGlhZ28tcmFtaXJleiIsImEiOiJja2ZqejYwcDAwczFzMnVsYnp3NjR5eGUyIn0.Sck7PoHPD0ivs7h1cc-RsQ'
        # fin address location
        latitude, longitude = self.address_to_lat_lng(address)

        # map box satellite image request url
        request_url = "https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/{},{},17.7,0,22/512x512@2x?access_token={}".format(longitude, latitude, access_token)

        # random names for files, will allow nultiple runs in parallel
        local_filename = str(uuid.uuid4().hex)
        map_pin_filename = str(uuid.uuid4().hex)
        transparent_map_pin_filename = str(uuid.uuid4().hex)

        # request and save satellite image
        urllib.request.urlretrieve(request_url, self.workspace + '\\satellite_images\\' + local_filename + '.png')

        # copy pin map image
        shutil.copy(self.workspace + '\\assets\\Capture.png', self.workspace + '\\assets\\'  +map_pin_filename + '.png')
        img = Image.open(self.workspace + '\\assets\\' + map_pin_filename + '.png')
        img = img.convert("RGBA")
        datas = img.getdata()
        
        # remove white pixels from pin map image
        newData = []
        for item in datas:
            if item[0] == 255 and item[1] == 255 and item[2] == 255:
                newData.append((255, 255, 255, 0))
            else:
                newData.append(item)

        img.putdata(newData)
        # save transaparent pin map image
        img.save(self.workspace + '\\assets\\' + transparent_map_pin_filename + '.png', "PNG")

        # Open images
        bg = Image.open(self.workspace + '\\satellite_images\\' + local_filename + '.png')
        fg = Image.open(self.workspace + '\\assets\\' + transparent_map_pin_filename + '.png').convert('RGBA')

        # Resize foreground down from 500x500 to 100x100
        fg_resized = fg.resize((82,82))

        # punt map pin in center of satellite image
        bg.paste(fg_resized,box=(452,445),mask=fg_resized)

        # Save result adnd remove original satellite image
        bg.save(self.workspace + '\\satellite_images\\' + address +'.jpg')
        fs.remove_file(self.workspace + '\\satellite_images\\' + local_filename + '.png')

        # validate if iamge is too blurry
        img = cv2.imread(self.workspace + '\\satellite_images\\'+ address  + '.jpg', cv2.IMREAD_GRAYSCALE)
        blurry_factor = cv2.Laplacian(img, cv2.CV_64F).var()
        if blurry_factor < 800:
            fs.remove_file(self.workspace + '\\satellite_images\\'+ address  + '.jpg')
            raise Exception('Image is too blurry')

        print('---Satellite image was downloaded successfully')
        shutil.move(self.workspace + '\\satellite_images\\' + address +'.jpg', 'C:\\Jenkins\\workspace\\TEST_2\\satellite_images\\'+ address +'.jpg')
        return (address + '.jpg')


    def address_to_lat_lng(self, address):
        # find longitude and lattitude from address using nominatim geolocator
        geolocator = Nominatim(user_agent='santiago.ramirez@teaminternational.com')
        location = geolocator.geocode(address)
        if location:
            return location.latitude, location.longitude
        else:
            raise Exception('Address could not be found by geolocator')


    def make_map_screenshot(self,city, address, state, zip_code):
        #number_image_name = randint(100, 999)
        #address_url = ''
        #for word in str(address).split():
            #address_url += word + '+'
        #map_url = '''https://www.google.com/maps/place/{}/data=
        #!3m1!1e3!4m5!3m4!1s0x872b0493375394f5:0x6d9013f2e59ccd84!8m2!3d33.3048185!4d-112.0160588'''.format(address_url+city+'+'+state+'+'+zip_code)
        browser = Browser()
        map_url = 'https://www.google.com/maps/@51.4788547,-0.0279647,35854m/data=!3m1!1e3'
        browser.open_chrome_browser(map_url, maximized=True)
        browser.input_text('id:searchboxinput', address + ', ' + city + ', ' +state+'\n')
        time.sleep(5)
        #element = browser.driver.find_element_by_xpath('//*[@id="widget-minimap-caption"]')
        #browser.driver.execute_script("arguments[0].click();", element)
        browser.driver.find_element_by_xpath('//body').screenshot(self.workspace + '\\satellite_images\\'+ address+','+city+','+state+','+zip_code+ '.png')

        browser.close_browser()
        shutil.move(self.workspace + '\\satellite_images\\'+ address+','+city+','+state+','+zip_code+ '.png', 'C:\\Jenkins\\workspace\\TEST_2\\satellite_images\\'+  address+','+city+','+state+','+zip_code+ '.png')
        print('---Google map screenshot saved')
        return (address+','+city+','+state+','+zip_code +'.png')
import sys, os
from RPA.FileSystem import FileSystem
from RPA.Email.ImapSmtp import ImapSmtp

arguments = sys.argv

email_sserver = arguments[1]
email_sport = int(arguments[2])
email_account = arguments[3]
email_password = arguments[4]

shared_folder = arguments[5]

fs = FileSystem()
input_files = fs.find_files('{}/**/address_verification_report.xlsx'.format(shared_folder))

for input_file in input_files:
    attach_file_path = input_file.path
    dir_name = os.path.dirname(attach_file_path)
    customer = os.path.split(dir_name)[-1]

    mail = ImapSmtp()
    mail.authorize_smtp(email_account, email_password, email_sserver, email_sport)

    mail.send_message(
        sender=email_account,
        recipients=customer,
        subject="Party address verification report",
        body='''Hi,
Please find summary report attached
Thank you,
RPA Team''',
        attachments = [attach_file_path]
    )

    print('---Email sent to: {}'.format(customer))